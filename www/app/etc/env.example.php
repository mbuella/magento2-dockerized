<?php
return array (
  'cache' => 
  array (
    'frontend' => 
    array (
      'default' => 
      array (
        'backend' => 'Cm_Cache_Backend_Redis',
        'backend_options' => 
        array (
          'server' => 'redis',
          'port' => '6379',
          'persistent' => '',
          'database' => '0',
          'password' => '',
          'force_standalone' => '0',
          'connect_retries' => '1',
          'read_timeout' => '10',
          'automatic_cleaning_factor' => '0',
          'compress_data' => '1',
          'compress_tags' => '1',
          'compress_threshold' => '20480',
          'compression_lib' => 'gzip',
          'use_lua' => '0',
        ),
      ),
      'page_cache' => 
      array (
        'backend' => 'Cm_Cache_Backend_Redis',
        'backend_options' => 
        array (
          'server' => 'redis',
          'port' => '6379',
          'persistent' => '',
          'database' => '1',
          'password' => '',
          'force_standalone' => '0',
          'connect_retries' => '1',
          'lifetimelimit' => '57600',
          'compress_data' => '0',
        ),
      ),
    ),
  ),
);
